import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { INITIALIZE_APP } from '../actions';

const propTypes = {
  children: PropTypes.node.isRequired,
  dispatch: PropTypes.func.isRequired,
};

class App extends React.Component {
  componentDidMount() {
    this.props.dispatch({
      type: INITIALIZE_APP,
    });
  }
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = propTypes;

const mapStateToProps = () => (console.log(''));

export default connect(mapStateToProps)(App);
