/* HomeView.jsx */

import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';

import { THEME } from '../utils';
import BlunderLeftDrawerContent from './BlunderLeftDrawerContent';
import BlunderRightDrawerContent from './BlunderRightDrawerContent';
import BlunderList from './BlunderList';

class HomeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLeftDrawer: true,
      showRightDrawer: false,
    };
  }

  closeItemDetail = () => {
    this.setState({ showRightDrawer: false });
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={THEME}>
        <div>
          <AppBar
            title="blunderlist"
            onLeftIconButtonTouchTap={() => this.setState({ showLeftDrawer: true })}
          />
          <div className="home-view">
            <div className="blunder-left-drawer">
              <Drawer
                open={this.state.showLeftDrawer}
              >
                <BlunderLeftDrawerContent
                  closeDrawer={() => this.setState({ showLeftDrawer: false })}
                  closeItemDetail={this.closeItemDetail}
                />
              </Drawer>
            </div>
            <BlunderList
              showItemDetail={() => this.setState({ showRightDrawer: true })}
              closeItemDetail={this.closeItemDetail}
            />
            <div className="blunder-right-drawer">
              <Drawer
                open={this.state.showRightDrawer}
                openSecondary
              >
                <BlunderRightDrawerContent
                  closeDrawer={this.closeItemDetail}
                />
              </Drawer>
            </div>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default HomeView;
