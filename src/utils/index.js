/* src/utils/index.js */

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import lightTheme from './lightTheme';

export const THEME = getMuiTheme({ ...lightTheme }); // eslint-disable-line
// export const THEME = getMuiTheme('./darkTheme'); // eslint-disable-line
