/* api/groups/index.js */
import 'whatwg-fetch';
import receiveStatus from '../receiveStatus';

const DOMAIN = 'http://localhost:8001';

export default class groupsApi {
  static getAllGroupsPromise() {
    return fetch(`${DOMAIN}/groups`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(res => receiveStatus(res))
      .then(res => res.json())
    .catch(err => receiveStatus(err));
  }

  static createGroupPromise(group) {
    return fetch(`${DOMAIN}/groups`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(group),
    })
      .then(res => receiveStatus(res))
        .then(res => res.json())
      .catch(err => receiveStatus(err));
  }

  static deleteGroupPromise(group) {
    return fetch(`${DOMAIN}/groups/${group.id}`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => receiveStatus(res))
        .then(res => res.json())
      .catch(err => receiveStatus(err));
  }

  static updateGroupPromise(group) {
    return fetch(`${DOMAIN}/groups/${group.id}`, {
      method: 'PUT',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(group),
    })
      .then(res => receiveStatus(res))
        .then(res => res.json())
      .catch(err => receiveStatus(err));
  }
}
